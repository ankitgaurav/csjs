function Graph(){
    let storage = {}
    return {
        contains: function(node){
            return !!storage[node]
        },
        addVertex: function(node){
            if(!this.contains(node)){
                storage[node] = {edges:{}}
            }
        },
        addEdge: function(startNode, endNode){
            if(this.contains(startNode) && this.contains(endNode)){
                storage[startNode].edges[endNode] = true
                storage[endNode].edges[startNode] = true
            }
        },
        removeEdge: function(startNode, endNode){
            if(this.contains(startNode) && this.contains(endNode)){
                delete storage[startNode].edges[endNode]
                delete storage[endNode].edges[startNode]
                return true
            }
        },
        removeVertex: function(node){
            if(this.contains(node)){
                for(let connectedNode in storage[node].edges){
                    this.removeEdge(node, connectedNode)
                }
                delete storage[node]
            }
        },
        hasEdge: function(startNode, endNode){
            if(this.contains(startNode) && this.contains(endNode)){
                return !!storage[startNode].edges[endNode]
            }
        },
        showGraph: function(){
            return storage
        }
    }
}
let myGraph = Graph()
myGraph.addVertex('Ankit')
myGraph.addVertex('Sidra')
myGraph.addVertex('Rashid')
myGraph.addVertex('Shubham')
myGraph.addVertex('Lovely')


myGraph.addEdge('Ankit', 'Sidra')
myGraph.addEdge('Ankit', 'Rashid')
myGraph.addEdge('Lovely', 'Lovely')
myGraph.addEdge('Rashid', 'Sidra')
myGraph.addEdge('Lovely', 'Shubham')
myGraph.addEdge('Lovely', 'Sidra')

console.log(myGraph.showGraph())
// myGraph.removeEdge('Ankit', 'Rashid')
// console.log(myGraph.showGraph())
// myGraph.removeVertex('Ankit')
// console.log(myGraph.showGraph())
console.log('Rashid is friends with Sidra? '+myGraph.hasEdge('Rashid', 'Sidra'))
console.log('Rashid is friends with Lovely? '+myGraph.hasEdge('Rashid', 'Lovely'))