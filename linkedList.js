const node = {
    init: function(value){
        this.value = value
        this.next = null
    }
}
const linkedList = {
    head: null,
    tail: null,
    length: 0,
    add: function(value){
        let newNode = Object.create(node)
        newNode.init(value)
        if(!this.head && !this.tail){//No item in linked list
            this.head = newNode
            this.tail = newNode
        } else {
            this.tail.next = newNode
            this.tail = newNode
        }
        this.length++;
    },
    contains: function(value){
        if(this.head.value === value || this.tail.value === value){
            return true
        }
        let previous = this.head
        let current = this.head
        while(current.next !== null){
            if(current.value === value){
                return true;
            }
            previous = current
            current = current.next
        }
        return false
    },
    remove: function(value){
        if(this.contains(value)){
            let previous = this.head
            let current = this.head
            while(current){
                if(current.value === value){//If the current node is to be removed
                    if(this.head === current){//If the current node is the head
                        this.head = current.next
                        this.length--
                        return
                    }
                    if(this.tail === current){//If the current item is the tail
                        this.tail = previous
                        this.length--
                        return
                    }
                    previous.next = current.next
                    this.length--
                    return
                }
                previous = current
                current = current.next
            }
        }
        return
    },
    size: function(){
        return this.length
    }
}
let myLinkedList = Object.create(linkedList)
myLinkedList.add('Raju')
myLinkedList.add('Ankit')
myLinkedList.add('Rashid')
myLinkedList.add('Pandey')
myLinkedList.add('Sidra')
console.log(myLinkedList)