function Stack(){
    let storage = {}
    let position = -1
    return {
        push: function(value){
            storage[++position] = value
        },
        pop: function(){
            const poppedVal = storage[position]
            position--
            return poppedVal
        },
        top: function(){
            return storage[position]
        },
        isEmpty: function(){
            return position === -1
        }
    }
}
const myStack = Stack()
myStack.push('Big')
myStack.push('Medium')
myStack.push('Small')
console.log(myStack)
while(!myStack.isEmpty()){
    console.log('Top element of stack is: '+myStack.top())
    console.log('Popped out: '+myStack.pop())
}