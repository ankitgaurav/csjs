const node = {
    init: function(data){
        this.data = data
        this.left = null
        this.right = null
    }
}
const BST = function(){
    let root = null
    return {
        insert: function(data){
            if (root !== null){
                currentNode = root
                while(true){
                    if(data<currentNode.data){//Goto left child
                        if(currentNode.left === null){
                            let newNode = Object.create(node)
                            newNode.init(data)
                            currentNode.left = newNode
                            return
                        } else {
                            currentNode = currentNode.left
                        }
                    }
                    else {//Goto right child
                        if(currentNode.right === null){
                            let newNode = Object.create(node)
                            newNode.init(data)
                            currentNode.right = newNode
                            return
                        } else {
                            currentNode = currentNode.right
                        }
                    }
                }
            } else {
                root = Object.create(node)
                root.init(data)
            }
        },
        search: function(value){
            function searchForData(node){
                if(node === null){
                    console.log(value+' not found in BST')
                    return
                }
                if(node.data === value){
                    console.log('Found '+value+' in BST')
                    return
                }
                if(value < node.data){
                    searchForData(node.left)
                } else if(value > node.data){
                    searchForData(node.right)
                }
            }
            return searchForData(root)
        },
        traverseInOrder: function(){
            function inOrder(node){
                if(node){
                    if(node.left !== null){
                        inOrder(node.left)
                    }
                    numbersInOrder.push(node.data)
                    if(node.right !== null){
                        inOrder(node.right)
                    }
                }
            }
            let numbersInOrder = []
            inOrder(root)
            console.log(numbersInOrder)
        },
        traversePostOrder: function(){
            console.log('Not yet complete')
        },
        traversePreOrder: function(){
            console.log('Not yet complete')
        },
        showRoot: function(){
            return root
        }
    }
}

const myBST = BST()
let arrayOfNumbers = [27, 14, 35, 10, 19, 31, 42, 3, 52, 6, 375]
for(let num of arrayOfNumbers){
    myBST.insert(num)
}
console.log(myBST.showRoot())
myBST.search(1000)
console.log(myBST.search(10))
